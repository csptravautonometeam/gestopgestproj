#tag Class
Protected Class RapportVisiteChantierClass
Inherits AllFormulaireClass
	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdTechEol, "rop0008_verif_hebdo_chef_chantier", "rop0008")
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerZones(formulaireDB As PostgreSQLDatabase, ByRef formulaire_tri_param As WebTextField, ByRef formulaire_cle_param As WebTextField, ByRef formulaire_desc_fr_param As WebTextField, ByRef formulaire_desc_en_param As WebTextField) As String
		  // Couper les zones à leur longeur maximum dans la BD
		  
		  //formulaire_cle_param.text =  Left(formulaire_cle_param.text,30)
		  //formulaire_desc_fr_param.text = Left(formulaire_desc_fr_param.text,100)
		  //formulaire_desc_en_param.text = Left(formulaire_desc_en_param.text,100)
		  
		  // Vérification des zones obligatoires
		  //If formulaire_tri_param.Text = "" Then formulaire_tri_param.Style = TextFieldErrorStyle
		  //If formulaire_cle_param.Text = "" Then formulaire_cle_param.Style = TextFieldErrorStyle
		  //If formulaire_desc_fr_param.Text = "" Then formulaire_desc_fr_param.Style = TextFieldErrorStyle
		  //If formulaire_desc_en_param.Text = "" Then formulaire_desc_en_param.Style = TextFieldErrorStyle
		  
		  //If formulaire_tri_param.Style = TextFieldErrorStyle Or formulaire_cle_param.Style = TextFieldErrorStyle Or formulaire_desc_fr_param.Style = TextFieldErrorStyle Or formulaire_desc_en_param.Style = TextFieldErrorStyle  Then
		  //Return FormulaireModule.ZONESOBLIGATOIRES
		  //End If
		  
		  // Vérification des zones numériques
		  //If formulaire_tri_param.text <> "" And  IsNumeric(formulaire_tri_param.text) = False Then formulaire_tri_param.Style = TextFieldErrorStyle
		  
		  //If formulaire_tri_param.Style = TextFieldErrorStyle Then
		  //Return FormulaireModule.ZONEDOITETRENUMERIQUE
		  //End If
		  
		  //formulaire_tri_param .text = str(Format( Val(formulaire_tri_param .text), "0"))
		  
		  Return  "Succes"
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		rop0008_commentaire_accompagnement_eolienne As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_commentaire_autre_visite As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_commentaire_avis_non_conformite As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_commentaire_client As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_commentaire_controle_qualite As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_commentaire_documents_remplis As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_commentaire_entretien_emplacements_fixes As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_commentaire_epi As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_commentaire_formation As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_commentaire_global As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_commentaire_intervention_employe As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_controle_qualite As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_documents_remplis As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_elingues As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_entretien_emplacements_fixes As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_entry_id As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_epi As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_formation As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_intervention_employe As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_projet As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_responsable_nas As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0008_revision As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="formulaire_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_form_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_accompagnement_eolienne"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_autre_visite"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_avis_non_conformite"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_client"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_controle_qualite"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_documents_remplis"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_entretien_emplacements_fixes"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_epi"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_formation"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_global"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_commentaire_intervention_employe"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_controle_qualite"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_documents_remplis"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_elingues"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_entretien_emplacements_fixes"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_entry_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_epi"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_formation"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_intervention_employe"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_responsable_nas"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0008_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
