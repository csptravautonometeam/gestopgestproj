#tag WebPage
Begin WebContainer AllFormulaireContainer
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   550
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   632
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h1
		Protected Sub loadDataEmploye(webPopupMenu_parm As WebPopupMenu)
		  Dim recordSet As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM employe ORDER BY employe_nom, employe_prenom "
		  
		  recordSet =  Session.bdTechEol.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  webPopupMenu_parm.DeleteAllRows
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    While Not recordSet.EOF
		      webPopupMenu_parm.AddRow(recordSet.Field( "employe_nom").StringValue + " " + recordSet.Field( "employe_prenom").StringValue)
		      // Mettre la valeur de la clé dans le RowTag
		      webPopupMenu_parm.RowTag(webPopupMenu_parm.ListCount-1) = recordSet.Field( "employe_nas").StringValue
		      recordSet.MoveNext
		    Wend
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		  //
		  //webPopupMenu_parm.InsertRow(0," ")
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub loadDataProjet(webPopupMenu_parm As WebPopupMenu)
		  Dim recordSet As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM projet WHERE projet_depot_note = 'Travaux en cours'  Order By projet_no "
		  
		  recordSet =  Session.bdTechEol.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  webPopupMenu_parm.DeleteAllRows
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    While Not recordSet.EOF
		      webPopupMenu_parm.AddRow(recordSet.Field( "projet_no").StringValue + " " + recordSet.Field( "projet_titre").StringValue)
		      // Mettre la valeur de la clé dans le RowTag
		      webPopupMenu_parm.RowTag(webPopupMenu_parm.ListCount-1) = recordSet.Field( "projet_no").StringValue
		      recordSet.MoveNext
		    Wend
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub majFormulaireDetail()
		  
		  
		  If Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.edit_mode = "Creation" Then
		    //alimListeDeroulante
		    // Assignation des propriétés aux Contrôles
		    Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.assignPropertiesToControls(Self, Self.prefix)
		  End If
		  
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.assignControlToProperties(Self, Self.prefix)
		  Dim messageErreur  As String = Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.writeData(Session.bdTechEol, Self.tableName, Self.prefix)
		  
		  If (messageErreur <> "Succes") Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = FormulaireModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit
		  End If
		  
		  // Réafficher la liste des formulaires si les valeurs ont changé
		  If (sauvegardeFormulaireDate <> Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.formulaire_date _
		    Or sauvegardeFormulaireProjet <> Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.formulaire_projet _
		    Or sauvegardeFormulaireRevision <>  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.formulaire_revision) _
		    And Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.edit_mode <> "Creation"  Then
		    Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.populateFormulaire(Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allformulaireStruct.no_ligne + 1)
		  End If
		  
		  sauvegardeFormulaireDate = Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.formulaire_date
		  sauvegardeFormulaireProjet = Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.formulaire_projet
		  sauvegardeFormulaireRevision =  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.formulaire_revision
		  
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allformulaireStruct.id = Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.formulaire_id
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allformulaireStruct.projet = Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.formulaire_projet
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allformulaireStruct.revision =  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.formulaire_revision
		  
		  If Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.edit_mode <> "Creation" Then
		    Dim ML6MajModalBox As New MajModal
		    ML6MajModalBox.Show
		  End If
		  
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.edit_mode = "Modification"
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub traiteMAJ()
		  If validationDesZonesOK = True Then
		    majFormulaireDetail
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validationDesZonesOK() As Boolean
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, False, True, False)
		  
		  Dim messageRetour as String = ""
		  
		  // Validation
		  //messageRetour = Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sFormulaire.validerZones( _
		  //Session.bdTechEol, _
		  //formulaire_tri_TextField, _
		  //formulaire_cle_TextField ,  _
		  //formulaire_desc_fr_TextField ,  _
		  //formulaire_desc_en_TextField)
		  //
		  //If messageRetour <> "Succes" Then
		  //Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		  //pointeurProbMsgModal.WLAlertLabel.Text = messageRetour
		  //pointeurProbMsgModal.Show
		  
		  //Return False
		  //End If
		  
		  Return True
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		prefix As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected sauvegardeFormulaireDate As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected sauvegardeFormulaireFormId As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected sauvegardeFormulaireProjet As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected sauvegardeFormulaireRevision As String
	#tag EndProperty

	#tag Property, Flags = &h0
		tableName As String
	#tag EndProperty


#tag EndWindowCode

#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="prefix"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="tableName"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
