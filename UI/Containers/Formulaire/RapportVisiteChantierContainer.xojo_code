#tag WebPage
Begin AllFormulaireContainer RapportVisiteChantierContainer
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   550
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   632
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin TestWebLabel rop0008_id_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   528
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   "Id"
      TextAlign       =   0
      Top             =   10
      VerticalCenter  =   0
      Visible         =   True
      Width           =   28
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin TestWebTextField rop0008_id_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   558
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   500
      Text            =   ""
      TextAlign       =   0
      Top             =   10
      Type            =   3
      VerticalCenter  =   0
      Visible         =   True
      Width           =   59
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField rop0008_date_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   179
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   5
      Text            =   ""
      TextAlign       =   0
      Top             =   69
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   111
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_date_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   179
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "Date"
      TextAlign       =   0
      Top             =   47
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_responsable_nas_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.RESPONSABLE"
      TextAlign       =   0
      Top             =   50
      VerticalCenter  =   0
      Visible         =   True
      Width           =   91
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu rop0008_responsable_nas_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   10
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1065947135"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   1
      Text            =   ""
      Top             =   72
      VerticalCenter  =   0
      Visible         =   True
      Width           =   157
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_epi_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.EPI"
      TextAlign       =   0
      Top             =   315
      VerticalCenter  =   0
      Visible         =   True
      Width           =   157
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup rop0008_epi_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#FormulaireModule.OUI`,`True`,``,`False`,`True`	`#FormulaireModule.NON`,`True`,``,`True`,`True`"
      Left            =   200
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   25
      Top             =   313
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TitreLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   203
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1056458751"
      TabOrder        =   1
      Text            =   "#FormulaireModule.RAPPORTVISITECHANTIER"
      TextAlign       =   0
      Top             =   4
      VerticalCenter  =   0
      Visible         =   True
      Width           =   266
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_global_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.COMMENTAIREGLOBAL"
      TextAlign       =   0
      Top             =   164
      VerticalCenter  =   0
      Visible         =   True
      Width           =   197
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_global_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   15
      Text            =   ""
      TextAlign       =   0
      Top             =   186
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_documents_remplis_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.DOCUMENTSREMPLIS"
      TextAlign       =   0
      Top             =   428
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup rop0008_documents_remplis_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#FormulaireModule.OUI`,`True`,``,`False`,`True`	`#FormulaireModule.NON`,`True`,``,`True`,`True`"
      Left            =   200
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   35
      Top             =   430
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRectangle SeparateurV1
      Cursor          =   0
      Enabled         =   True
      Height          =   502
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   313
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1719576575"
      TabOrder        =   -1
      Top             =   42
      VerticalCenter  =   0
      Visible         =   True
      Width           =   1
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_epi_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.COMMENTAIRE"
      TextAlign       =   0
      Top             =   338
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_epi_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   30
      Text            =   ""
      TextAlign       =   0
      Top             =   360
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_documents_remplis_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.COMMENTAIRE"
      TextAlign       =   0
      Top             =   456
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_documents_remplis_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   40
      Text            =   ""
      TextAlign       =   0
      Top             =   476
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_accompagnement_eolienne_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.ACCOMPAGNEMENTEOLIENNE"
      TextAlign       =   0
      Top             =   47
      VerticalCenter  =   0
      Visible         =   True
      Width           =   165
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup rop0008_accompagnement_eolienne_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#FormulaireModule.OUI`,`True`,``,`False`,`True`	`#FormulaireModule.NON`,`True`,``,`True`,`True`"
      Left            =   505
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   45
      Top             =   49
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_accompagnement_eolienne_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.COMMENTAIRE"
      TextAlign       =   0
      Top             =   79
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_accompagnement_eolienne_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   50
      Text            =   ""
      TextAlign       =   0
      Top             =   101
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_controle_qualite_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.CONTROLEQUALITE"
      TextAlign       =   0
      Top             =   175
      VerticalCenter  =   0
      Visible         =   True
      Width           =   164
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup rop0008_controle_qualite_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#FormulaireModule.OUI`,`True`,``,`False`,`True`	`#FormulaireModule.NON`,`True`,``,`True`,`True`"
      Left            =   505
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   55
      Top             =   177
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_controle_qualite_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.COMMENTAIRE"
      TextAlign       =   0
      Top             =   208
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_controle_qualite_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   60
      Text            =   ""
      TextAlign       =   0
      Top             =   230
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_entretien_emplacements_fixes_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.ENTRETIENEMPLACEMENTSFIXES"
      TextAlign       =   0
      Top             =   311
      VerticalCenter  =   0
      Visible         =   True
      Width           =   164
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup rop0008_entretien_emplacements_fixes_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#FormulaireModule.OUI`,`True`,``,`False`,`True`	`#FormulaireModule.NON`,`True`,``,`True`,`True`"
      Left            =   505
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   65
      Top             =   313
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_entretien_emplacements_fixes_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.COMMENTAIRE"
      TextAlign       =   0
      Top             =   337
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_entretien_emplacements_fixes_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   70
      Text            =   ""
      TextAlign       =   0
      Top             =   359
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_client_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.CLIENT"
      TextAlign       =   0
      Top             =   428
      VerticalCenter  =   0
      Visible         =   True
      Width           =   164
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup rop0008_client_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#FormulaireModule.OUI`,`True`,``,`False`,`True`	`#FormulaireModule.NON`,`True`,``,`True`,`True`"
      Left            =   505
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   75
      Top             =   430
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_client_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.COMMENTAIRE"
      TextAlign       =   0
      Top             =   454
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_client_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   325
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   80
      Text            =   ""
      TextAlign       =   0
      Top             =   476
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_formation_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   656
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.FORMATION"
      TextAlign       =   0
      Top             =   46
      VerticalCenter  =   0
      Visible         =   True
      Width           =   164
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup rop0008_formation_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#FormulaireModule.OUI`,`True`,``,`False`,`True`	`#FormulaireModule.NON`,`True`,``,`True`,`True`"
      Left            =   836
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   85
      Top             =   48
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_formation_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   656
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.COMMENTAIRE"
      TextAlign       =   0
      Top             =   79
      VerticalCenter  =   0
      Visible         =   True
      Width           =   93
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_formation_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   656
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   90
      Text            =   ""
      TextAlign       =   0
      Top             =   101
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_intervention_employe_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   34
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   656
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.INTERVENTIONEMPLOYE"
      TextAlign       =   0
      Top             =   193
      VerticalCenter  =   0
      Visible         =   True
      Width           =   282
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_intervention_employe_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   656
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   100
      Text            =   ""
      TextAlign       =   0
      Top             =   232
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_autre_visite_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   34
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   656
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.AUTRECOMMENTAIRE"
      TextAlign       =   0
      Top             =   320
      VerticalCenter  =   0
      Visible         =   True
      Width           =   282
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_autre_visite_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   656
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   110
      Text            =   ""
      TextAlign       =   0
      Top             =   359
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_commentaire_avis_non_conformite_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   34
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   656
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.AVISNONCONFORMITE"
      TextAlign       =   0
      Top             =   438
      VerticalCenter  =   0
      Visible         =   True
      Width           =   282
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea rop0008_commentaire_avis_non_conformite_TextArea
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   62
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   656
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1065947135"
      TabOrder        =   120
      Text            =   ""
      TextAlign       =   0
      Top             =   476
      VerticalCenter  =   0
      Visible         =   True
      Width           =   292
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSegmentedControl rop0008_SegmentedControl
      Cursor          =   0
      Enabled         =   True
      Height          =   15
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "P1	True	True	0		P2	False	True	0"
      Left            =   10
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      SegmentCount    =   2
      SelectedStyle   =   "0"
      SelectionType   =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   16
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel rop0008_revision_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   True
      Scope           =   0
      Style           =   "1065947135"
      TabOrder        =   1
      Text            =   "#FormulaireModule.REVISE"
      TextAlign       =   0
      Top             =   117
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup rop0008_revision_RadioGroup
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`#FormulaireModule.OUI`,`True`,``,`False`,`True`	`#FormulaireModule.NON`,`True`,``,`True`,`True`"
      Left            =   200
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "0"
      TabOrder        =   10
      Top             =   115
      VerticalCenter  =   0
      Visible         =   True
      Width           =   102
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator SeparateurH2
      Cursor          =   0
      Enabled         =   True
      Height          =   4
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   -1
      Top             =   304
      VerticalCenter  =   0
      Visible         =   True
      Width           =   607
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator SeparateurH1
      Cursor          =   0
      Enabled         =   True
      Height          =   2
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   313
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   -1
      Top             =   175
      VerticalCenter  =   0
      Visible         =   True
      Width           =   304
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator SeparateurH3
      Cursor          =   0
      Enabled         =   True
      Height          =   4
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   -1
      Top             =   430
      VerticalCenter  =   0
      Visible         =   True
      Width           =   607
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebSeparator SeparateurH4
      Cursor          =   0
      Enabled         =   True
      Height          =   2
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   656
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   2
      Style           =   "0"
      TabOrder        =   -1
      Top             =   175
      VerticalCenter  =   0
      Visible         =   True
      Width           =   304
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub affichePage1()
		  // Cacher la page 2
		  rop0008_formation_Label.Left = 10
		  rop0008_formation_Label.Visible = False
		  rop0008_formation_RadioGroup.Left = 200
		  rop0008_formation_RadioGroup.Visible = False
		  rop0008_commentaire_formation_Label.Left = 10
		  rop0008_commentaire_formation_Label.Visible = False
		  rop0008_commentaire_formation_TextArea.Left =10
		  rop0008_commentaire_formation_TextArea.Visible = False
		  
		  SeparateurH4.Left = 10
		  SeparateurH4.Visible = False
		  
		  rop0008_commentaire_intervention_employe_Label.Left = 10
		  rop0008_commentaire_intervention_employe_Label.Visible = False
		  rop0008_commentaire_intervention_employe_TextArea.Left = 10
		  rop0008_commentaire_intervention_employe_TextArea.Visible = False
		  
		  rop0008_commentaire_autre_visite_Label.Left = 10
		  rop0008_commentaire_autre_visite_Label.Visible = False
		  rop0008_commentaire_autre_visite_TextArea.Left = 10
		  rop0008_commentaire_autre_visite_TextArea.Visible = False
		  
		  rop0008_commentaire_avis_non_conformite_Label.Left = 10
		  rop0008_commentaire_avis_non_conformite_Label.Visible = False
		  rop0008_commentaire_avis_non_conformite_TextArea.Left = 10
		  rop0008_commentaire_avis_non_conformite_TextArea.Visible = False
		  
		  
		  // Afficher la page 1
		  rop0008_responsable_nas_Label.Visible = True
		  rop0008_responsable_nas_PopupMenu.Visible = True
		  rop0008_date_Label.Visible = True
		  rop0008_date_TextField.Visible = True
		  //rop0008_projet_Label.Visible = True
		  //rop0008_projet_PopupMenu.Visible = True
		  rop0008_commentaire_global_Label.Visible = True
		  rop0008_commentaire_global_TextArea.Visible = True
		  rop0008_revision_Label.Visible = True
		  rop0008_revision_RadioGroup.Visible = True
		  
		  rop0008_epi_Label.Visible = True
		  rop0008_epi_RadioGroup.Visible = True
		  rop0008_commentaire_epi_Label.Visible = True
		  rop0008_commentaire_epi_TextArea.Visible = True
		  
		  rop0008_documents_remplis_Label.Visible = True
		  rop0008_documents_remplis_RadioGroup.Visible = True
		  rop0008_commentaire_documents_remplis_Label.Visible = True
		  rop0008_commentaire_documents_remplis_TextArea.Visible = True
		  
		  rop0008_accompagnement_eolienne_Label.Visible = True
		  rop0008_accompagnement_eolienne_RadioGroup.Visible = True
		  rop0008_commentaire_accompagnement_eolienne_Label.Visible = True
		  rop0008_commentaire_accompagnement_eolienne_TextArea.Visible = True
		  
		  rop0008_controle_qualite_Label.Visible = True
		  rop0008_controle_qualite_RadioGroup.Visible = True
		  rop0008_commentaire_controle_qualite_Label.Visible = True
		  rop0008_commentaire_controle_qualite_TextArea.Visible = True
		  
		  rop0008_entretien_emplacements_fixes_Label.Visible = True
		  rop0008_entretien_emplacements_fixes_RadioGroup.Visible = True
		  rop0008_commentaire_entretien_emplacements_fixes_Label.Visible = True
		  rop0008_commentaire_entretien_emplacements_fixes_TextArea.Visible = True
		  
		  rop0008_client_Label.Visible = True
		  rop0008_client_RadioGroup.Visible = True
		  rop0008_commentaire_client_Label.Visible = True
		  rop0008_commentaire_client_TextArea.Visible = True
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub affichePage2()
		  
		  // Cacher la page 1
		  rop0008_responsable_nas_Label.Visible = False
		  rop0008_responsable_nas_PopupMenu.Visible = False
		  rop0008_date_Label.Visible = False
		  rop0008_date_TextField.Visible = False
		  //rop0008_projet_Label.Visible = False
		  //rop0008_projet_PopupMenu.Visible = False
		  rop0008_commentaire_global_Label.Visible = False
		  rop0008_commentaire_global_TextArea.Visible = False
		  rop0008_revision_Label.Visible = False
		  rop0008_revision_RadioGroup.Visible = False
		  
		  rop0008_epi_Label.Visible = False
		  rop0008_epi_RadioGroup.Visible = False
		  rop0008_commentaire_epi_Label.Visible = False
		  rop0008_commentaire_epi_TextArea.Visible = False
		  
		  rop0008_documents_remplis_Label.Visible = False
		  rop0008_documents_remplis_RadioGroup.Visible = False
		  rop0008_commentaire_documents_remplis_Label.Visible = False
		  rop0008_commentaire_documents_remplis_TextArea.Visible = False
		  
		  rop0008_accompagnement_eolienne_Label.Visible = False
		  rop0008_accompagnement_eolienne_RadioGroup.Visible = False
		  rop0008_commentaire_accompagnement_eolienne_Label.Visible = False
		  rop0008_commentaire_accompagnement_eolienne_TextArea.Visible = False
		  
		  rop0008_controle_qualite_Label.Visible = False
		  rop0008_controle_qualite_RadioGroup.Visible = False
		  rop0008_commentaire_controle_qualite_Label.Visible = False
		  rop0008_commentaire_controle_qualite_TextArea.Visible = False
		  
		  rop0008_entretien_emplacements_fixes_Label.Visible = False
		  rop0008_entretien_emplacements_fixes_RadioGroup.Visible = False
		  rop0008_commentaire_entretien_emplacements_fixes_Label.Visible = False
		  rop0008_commentaire_entretien_emplacements_fixes_TextArea.Visible = False
		  
		  rop0008_client_Label.Visible = False
		  rop0008_client_RadioGroup.Visible = False
		  rop0008_commentaire_client_Label.Visible = False
		  rop0008_commentaire_client_TextArea.Visible = False
		  
		  // Afficher la page 2
		  
		  rop0008_formation_Label.Left = 10
		  rop0008_formation_Label.Visible = True
		  rop0008_formation_RadioGroup.Left = 200
		  rop0008_formation_RadioGroup.Visible = True
		  rop0008_commentaire_formation_Label.Left = 10
		  rop0008_commentaire_formation_Label.Visible = True
		  rop0008_commentaire_formation_TextArea.Left =10
		  rop0008_commentaire_formation_TextArea.Visible = True
		  
		  SeparateurH4.Left = 10
		  SeparateurH4.Visible = True
		  
		  rop0008_commentaire_intervention_employe_Label.Left = 10
		  rop0008_commentaire_intervention_employe_Label.Visible = True
		  rop0008_commentaire_intervention_employe_TextArea.Left = 10
		  rop0008_commentaire_intervention_employe_TextArea.Visible = True
		  
		  rop0008_commentaire_autre_visite_Label.Left = 10
		  rop0008_commentaire_autre_visite_Label.Visible = True
		  rop0008_commentaire_autre_visite_TextArea.Left = 10
		  rop0008_commentaire_autre_visite_TextArea.Visible = True
		  
		  rop0008_commentaire_avis_non_conformite_Label.Left = 10
		  rop0008_commentaire_avis_non_conformite_Label.Visible = True
		  rop0008_commentaire_avis_non_conformite_TextArea.Left = 10
		  rop0008_commentaire_avis_non_conformite_TextArea.Visible = True
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub listFormulaireDetail()
		  // Si le record n'est pas vérrouillé, on déverrouille pour permettre la modification
		  If Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.lockEditMode Then 
		    // Paramètre 1 = WebView
		    // Paramètre 2 = Initialisation du contenu des zones
		    // Paramètre 3 = Initialisation des styles
		    // Paramètre 4 = Verrouillage des zones
		    GenControlModule.resetControls(Self, False, False, False)
		    // Reverrouiller certaines zones 
		    rop0008_responsable_nas_PopupMenu.Enabled = False
		    rop0008_date_TextField.Enabled = False
		    //rop0008_projet_PopupMenu.Enabled = False
		    rop0008_commentaire_global_TextArea.SetFocus
		    Exit Sub
		  End If
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, True, True, True)
		  
		  //If Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sFormulaire = Nil Then
		  //Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sFormulaire = new FormulaireClass()
		  //End If
		  
		  If Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.edit_mode = "Creation" Then
		    Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.edit_mode = "Modification"
		    //formulaire_nas_TextField.Text =Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.formulaireStruct.nas
		    //formulaire_form_id_TextField.Text =Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.formulaireStruct.nom
		    //formulaire_prenom_TextField.Text =Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.formulaireStruct.prenom
		    Exit
		  End If
		  
		  // Alimenter les listes déroulantes
		  loadDataEmploye(rop0008_responsable_nas_PopupMenu)
		  //loadDataProjet(rop0008_projet_PopupMenu)
		  
		  // Mode Modification, Lire les données
		  Dim formulaireRS As RecordSet = Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.loadDataByField(Session.bdTechEol, Self.tableName, _
		  Self.prefix + "_id", str(Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.id), Self.prefix + "_id")
		  If formulaireRS <> Nil Then Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.LireDonneesBD(formulaireRS, Self.prefix)
		  
		  // Sauvegarde
		  sauvegardeFormulaireProjet = formulaireRS.Field(Self.prefix + "_projet").StringValue
		  sauvegardeFormulaireDate = formulaireRS.Field(Self.prefix + "_date").StringValue
		  sauvegardeFormulaireFormId = formulaireRS.Field("form_id").StringValue
		  sauvegardeFormulaireRevision = formulaireRS.Field(Self.prefix + "_revision").StringValue
		  
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.id = formulaireRS.Field(Self.prefix + "_id").IntegerValue
		  
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allFormulaireStruct.id = formulaireRS.Field(Self.prefix + "_id").IntegerValue
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allformulaireStruct.date = formulaireRS.Field(Self.prefix + "_date").StringValue
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allformulaireStruct.projet = formulaireRS.Field(Self.prefix + "_projet").StringValue
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.allformulaireStruct.revision = formulaireRS.Field(Self.prefix + "_revision").StringValue
		  formulaireRS.Close
		  formulaireRS = Nil
		  
		  // Assignation des propriétés aux Contrôles
		  Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.sAllFormulaire.assignPropertiesToControls(Self, Self.prefix)
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, False, False, True)
		  
		  // Composantes à débloquer
		  rop0008_SegmentedControl.Enabled = True
		  
		  
		End Sub
	#tag EndMethod


#tag EndWindowCode

#tag Events rop0008_date_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events rop0008_SegmentedControl
	#tag Event
		Sub Action(SegmentIndex As Integer)
		  
		  Select Case SegmentIndex
		  Case 0 // Page 1
		    affichePage1
		    
		  Case 1 // Page 2
		    affichePage2
		    
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="prefix"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="tableName"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
