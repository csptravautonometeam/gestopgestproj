#tag Class
Protected Class RapportHebdoClass
Inherits AllFormulaireClass
	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdTechEol, "rop0009_rapport_hebdomadaire", "rop0009")
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerZones(formulaireDB As PostgreSQLDatabase, ByRef formulaire_tri_param As WebTextField, ByRef formulaire_cle_param As WebTextField, ByRef formulaire_desc_fr_param As WebTextField, ByRef formulaire_desc_en_param As WebTextField) As String
		  // Couper les zones à leur longeur maximum dans la BD
		  
		  formulaire_cle_param.text =  Left(formulaire_cle_param.text,30)
		  formulaire_desc_fr_param.text = Left(formulaire_desc_fr_param.text,100)
		  formulaire_desc_en_param.text = Left(formulaire_desc_en_param.text,100)
		  
		  // Vérification des zones obligatoires
		  If formulaire_tri_param.Text = "" Then formulaire_tri_param.Style = TextFieldErrorStyle
		  If formulaire_cle_param.Text = "" Then formulaire_cle_param.Style = TextFieldErrorStyle
		  If formulaire_desc_fr_param.Text = "" Then formulaire_desc_fr_param.Style = TextFieldErrorStyle
		  If formulaire_desc_en_param.Text = "" Then formulaire_desc_en_param.Style = TextFieldErrorStyle
		  
		  If formulaire_tri_param.Style = TextFieldErrorStyle Or formulaire_cle_param.Style = TextFieldErrorStyle Or formulaire_desc_fr_param.Style = TextFieldErrorStyle Or formulaire_desc_en_param.Style = TextFieldErrorStyle  Then
		    Return FormulaireModule.ZONESOBLIGATOIRES
		  End If
		  
		  // Vérification des zones numériques
		  If formulaire_tri_param.text <> "" And  IsNumeric(formulaire_tri_param.text) = False Then formulaire_tri_param.Style = TextFieldErrorStyle
		  
		  If formulaire_tri_param.Style = TextFieldErrorStyle Then
		    Return FormulaireModule.ZONEDOITETRENUMERIQUE
		  End If
		  
		  formulaire_tri_param .text = str(Format( Val(formulaire_tri_param .text), "0"))
		  
		  Return  "Succes"
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		rop0009_accident As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_autres_commentaires_travaux As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_deficiency_log_a_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_docum_sites_complet_envoye As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_entry_id As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_feuille_securite As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_inspection_hebdomadaire As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_inspection_vehicule As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_liste_pieces_pris_a_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_near_miss As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_outils As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_personnel As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_photos_problemes_tours_remis As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_produits As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_projet As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_rapport_securite As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_responsable_ehs_nas As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_responsable_nas As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_revision As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_semaine_finissant_le As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_total_heures_facturables As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_total_tours_completes As String
	#tag EndProperty

	#tag Property, Flags = &h0
		rop0009_total_tours_completes_semaine As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="formulaire_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_form_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_accident"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_autres_commentaires_travaux"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_deficiency_log_a_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_docum_sites_complet_envoye"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_entry_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_feuille_securite"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_inspection_hebdomadaire"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_inspection_vehicule"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_liste_pieces_pris_a_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_near_miss"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_outils"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_personnel"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_photos_problemes_tours_remis"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_produits"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_rapport_securite"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_responsable_ehs_nas"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_responsable_nas"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_semaine_finissant_le"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_total_heures_facturables"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_total_tours_completes"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="rop0009_total_tours_completes_semaine"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
