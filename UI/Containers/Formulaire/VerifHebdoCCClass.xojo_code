#tag Class
Protected Class VerifHebdoCCClass
Inherits AllFormulaireClass
	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdTechEol, "fss0006_verif_hebdo_chef_chantier", "fss0006")
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerZones(formulaireDB As PostgreSQLDatabase, ByRef formulaire_tri_param As WebTextField, ByRef formulaire_cle_param As WebTextField, ByRef formulaire_desc_fr_param As WebTextField, ByRef formulaire_desc_en_param As WebTextField) As String
		  // Couper les zones à leur longeur maximum dans la BD
		  
		  //formulaire_cle_param.text =  Left(formulaire_cle_param.text,30)
		  //formulaire_desc_fr_param.text = Left(formulaire_desc_fr_param.text,100)
		  //formulaire_desc_en_param.text = Left(formulaire_desc_en_param.text,100)
		  
		  // Vérification des zones obligatoires
		  //If formulaire_tri_param.Text = "" Then formulaire_tri_param.Style = TextFieldErrorStyle
		  //If formulaire_cle_param.Text = "" Then formulaire_cle_param.Style = TextFieldErrorStyle
		  //If formulaire_desc_fr_param.Text = "" Then formulaire_desc_fr_param.Style = TextFieldErrorStyle
		  //If formulaire_desc_en_param.Text = "" Then formulaire_desc_en_param.Style = TextFieldErrorStyle
		  
		  //If formulaire_tri_param.Style = TextFieldErrorStyle Or formulaire_cle_param.Style = TextFieldErrorStyle Or formulaire_desc_fr_param.Style = TextFieldErrorStyle Or formulaire_desc_en_param.Style = TextFieldErrorStyle  Then
		  //Return FormulaireModule.ZONESOBLIGATOIRES
		  //End If
		  
		  // Vérification des zones numériques
		  //If formulaire_tri_param.text <> "" And  IsNumeric(formulaire_tri_param.text) = False Then formulaire_tri_param.Style = TextFieldErrorStyle
		  
		  //If formulaire_tri_param.Style = TextFieldErrorStyle Then
		  //Return FormulaireModule.ZONEDOITETRENUMERIQUE
		  //End If
		  
		  //formulaire_tri_param .text = str(Format( Val(formulaire_tri_param .text), "0"))
		  
		  Return  "Succes"
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		fss0006_commentaire_defibrillateur As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_elingues As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_entretien_emplacements_fixes As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_epi As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_equip_outils As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_global As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_harnais As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_kit_arc_flash As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_kit_sauvetage As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_lift_bags As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_remorque As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_trousse_pelican As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_trousse_prem_soins As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_commentaire_trousse_sos_extincteur As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_date As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_defibrillateur As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_elingues As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_entretien_emplacements_fixes As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_entry_id As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_epi As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_equip_outils As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_harnais As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_kit_arc_flash As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_kit_sauvetage As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_lift_bags As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_projet As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_remorque As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_responsable_nas As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_revision As String
	#tag EndProperty

	#tag Property, Flags = &h0
		fss0006_trousse_pelican As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="formulaire_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_form_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="formulaire_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_defibrillateur"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_elingues"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_entretien_emplacements_fixes"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_epi"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_equip_outils"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_global"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_harnais"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_kit_arc_flash"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_kit_sauvetage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_lift_bags"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_remorque"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_trousse_pelican"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_trousse_prem_soins"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_commentaire_trousse_sos_extincteur"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_date"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_defibrillateur"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_elingues"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_entretien_emplacements_fixes"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_entry_id"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_epi"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_equip_outils"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_harnais"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_kit_arc_flash"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_kit_sauvetage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_lift_bags"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_remorque"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_responsable_nas"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_revision"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="fss0006_trousse_pelican"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
