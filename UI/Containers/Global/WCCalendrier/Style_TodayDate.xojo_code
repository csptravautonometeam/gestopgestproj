#tag WebStyle
WebStyle Style_TodayDate
Inherits WebStyle
	#tag WebStyleStateGroup
		text-align=center
		text-decoration=True false false false false
		text-color=FFFFFFFF
		border-left=1px solid 8C8C8CFF
		border-bottom=1px solid CCCCCCFF
		border-right=1px solid CCCCCCFF
		misc-background=gradient vertical 0 FF8000FF 1 FF0000FF
		text-size=10px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=solid 5676A7FF
		text-color=FFFFFFFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=gradient vertical 0 F5F5F5FF 1 2B2B2BFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle Style_TodayDate
#tag EndWebStyle

