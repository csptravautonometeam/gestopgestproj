#tag Class
Protected Class ProjetClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdTechEol, "projet", "projet")
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerSuppression(projetDB As PostgreSQLDatabase, table_nom As String, value As String) As String
		  // Vérifier si l'enregistrement sélectionné peut être supprimé
		  Dim recordSet as RecordSet
		  Dim strSQL As String = ""
		  
		  Select Case table_nom
		    
		    //Case "reppale_descanomalie"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		    //Case "reppale_etat"
		    //strSQL = "SELECT * FROM reppale WHERE reppale_rep_etat = '" + value + "'"
		    //Case "reppale_gravite"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_gravite = '" + value + "'"
		    //Case "reppale_intervention"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_intervention = '" + value + "'"
		    //Case "reppale_acces"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_methode_acces = '" + value + "'"
		    //Case "reppale_pospremiere"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_position_premiere = '" + value + "'"
		    //Case "reppale_posseconde"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_position_seconde = '" + value + "'"
		    //Case "reppale_recouvrement"
		    //strSQL = "SELECT * FROM reppale WHERE reppale_recouvrement = '" + value + "'"
		    //If value = "NS" Then strSQL = "SELECT * FROM reppale WHERE reppale_recouvrement = '' Or  reppale_recouvrement = '" + value + "'"
		    //Case "responsable"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_responsable = '" + value + "'"
		    //Case "sitecode"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_site = '" + value + "'"
		    //Case "reppale_statut"
		    //strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_statut = '" + value + "'"
		    //Case "reppale_typeanomalie"
		    // strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		    
		  End Select
		  
		  recordSet =  projetDB .SQLSelect(strSQL)
		  Dim compteur As Integer = recordSet.RecordCount
		  
		  If compteur > 0 Then
		    //Return ProjetModule.ENREGNEPEUTPASETREDETRUIT
		  Else
		    Return  "Succes"
		  End If
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		projet_charge_projet As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_charge_projet_nom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_client As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_client_coordonnees As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_client_nom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_client_site_adresse As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_client_site_contact As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_date_debut_prevue As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_date_debut_reelle As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_date_fin_prevue As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_date_fin_reelle As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_estimateur As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_estimateur_nom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_no As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_statut As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_titre As String
	#tag EndProperty

	#tag Property, Flags = &h0
		projet_type As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_charge_projet"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_charge_projet_nom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_client"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_client_coordonnees"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_client_nom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_client_site_adresse"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_client_site_contact"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_date_debut_prevue"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_date_debut_reelle"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_date_fin_prevue"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_date_fin_reelle"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_estimateur"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_estimateur_nom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_no"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_statut"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_titre"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="projet_type"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
