#tag WebPage
Begin WebContainer ProjetSideBar
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   640
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "0"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   350
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebListBox ProjetListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   2
      ColumnWidths    =   "30%,70%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "1065947135"
      Height          =   585
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "#ProjetModule.NOPROJET	#ProjetModule.NOMPROJET		"
      Left            =   0
      ListIndex       =   -1
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "1065947135"
      Style           =   "1065947135"
      TabOrder        =   -1
      Top             =   55
      VerticalCenter  =   0
      Visible         =   True
      Width           =   350
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ProjetLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   ""
      TextAlign       =   0
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   210
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVAddProjet
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   30
      HelpTag         =   "#ProjetModule.HINTADD"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   238
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1401872383
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVDelProjet
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   30
      HelpTag         =   "#ProjetModule.HINTSUPPRIMER"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   272
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   415664127
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVEnrProjet
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   30
      HelpTag         =   "#ProjetModule.HINTUPDATEDETAILS"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   272
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1113270271
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TitreTableLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1056458751"
      TabOrder        =   1
      Text            =   "#ProjetModule.PROJET"
      TextAlign       =   0
      Top             =   12
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVModProjet
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#ProjetModule.HINTLOCKUNLOCK"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   238
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1238587391
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVRafProjet
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   312
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   2017697791
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckbox ProjetEnCoursCheckbox
      Caption         =   "#ProjetModule.ENCOURS"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   103
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   5
      Value           =   True
      VerticalCenter  =   0
      Visible         =   True
      Width           =   98
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebCheckbox ProjetAPlanifierCheckbox
      Caption         =   "#ProjetModule.APLANIFIER"
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   103
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Top             =   31
      Value           =   True
      VerticalCenter  =   0
      Visible         =   True
      Width           =   98
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ajouterProjet()
		  Dim compteur As Integer
		  compteur = ProjetListBox.RowCount  'Pour Debug
		  compteur = ProjetListBox.LastIndex    'Pour Debug
		  compteur = ProjetListBox.ListIndex ' Pour Debug
		  
		  //ProjetListBox.insertRow(compteur+1, ProjetModule.NOUVEAUNAS, ProjetModule.NOUVEAUNOM, ProjetModule.NOUVEAUPRENOM, "A")
		  
		  // Faire pointer le curseur sur le nouveal projet
		  ProjetListBox.ListIndex = ProjetListBox.ListIndex + 1
		  Session.pointeurProjetContainerControl.ProjetSideBarDialog.ProjetStruct.no_ligne = ProjetListBox.ListIndex
		  
		  //Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.nas = ProjetModule.NOUVEAUNAS
		  //Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.nom = ProjetModule.NOUVEAUNOM
		  //Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.prenom = ProjetModule.NOUVEAUPRENOM
		  Session.pointeurProjetContainerControl.ProjetSideBarDialog.ProjetStruct.revision = ""
		  Session.pointeurProjetContainerControl.ProjetSideBarDialog.ProjetStruct.edit_mode = "Creation"
		  
		  // Création d'un nouvel enregistrement
		  Self.sProjet = Nil
		  //Self.sProjet = new ProjetClass()
		  //Self.sProjet.projet_nas = ProjetModule.NOUVEAUNAS
		  //Self.sProjet.projet_form_id = ProjetModule.NOUVEAUNOM
		  //Self.sProjet.projet_prenom = ProjetModule.NOUVEAUPRENOM
		  //Self.sProjet.projet_statut = "A"
		  // projet_cleetr_nom et projet_cleetr_cle vont prendre la valeur de la ligne précédente
		  
		  Session.pointeurProjetContainerControl.ProjetSideBarDialog.ProjetStruct.id = 0  // indique que c'est un nouvel enregistrement
		  //Self.sProjet.projet_id = 0
		  //Session.pointeurProjetContainerControl.ProjetContainerDialog.majProjetDetail
		  
		  // Renumérotation de la listbox et de la table paramètre
		  //renumerotation
		  
		  // Réafficher tout et se repositionner à la ligne ajoutée
		  //populateProjet(Self.sProjet.projet_id)
		  
		  // Se mettre en mode lock
		  //implementLock
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub changementProjet(idProjet_param As Integer)
		  // Rendre invisible les informations de blocage
		  Session.pointeurProjetContainerControl.IVLockedProjet.Visible = False
		  Session.pointeurProjetContainerControl.TLLockedByProjet.Text = ""
		  Session.pointeurProjetContainerControl.TLLockedByProjet.Visible = False
		  
		  // Si on est en verrouillage et qu'il y a changement d'enregistrement, on remet les icones pertinents actifs et on bloque l'écran Container 
		  If Self.sProjet.lockEditMode = True Then
		    Self.sProjet.lockEditMode = False
		    // Enlever les verrouillages existants
		    Dim message As String = Self.sProjet.cleanUserLocks(Session.bdTechEol)
		    If  message <> "Succes" Then afficherMessage(message)
		    
		    // Rendre indisponible l'ajout et la suppression
		    IVModProjet.Picture = ModModal30x30
		    //IVAddProjet.Visible = True
		    //IVAddProjet.Enabled = True
		    //IVDelProjet.Visible = True
		    //IVDelProjet.Enabled = True
		    IVEnrProjet.Visible = False
		    IVEnrProjet.Enabled = False
		  End If
		  
		  // Si la valeur de idProjet est 0, c'est un nouvel enregistrement, donc pas de lecture de la BD
		  //If idProjet_param  = 0 Then
		  //Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.edit_mode = "Creation"
		  //Exit Sub
		  //End If
		  
		  // Restaurer la valeur de la clé du record 
		  projetStruct.id = ProjetListBox.CellTag(idProjet_param,1)
		  
		  // Libérer l'espace mémoire si c'est le cas
		  If Session.pointeurAllProjetContainer <> Nil Then
		    Session.pointeurAllProjetContainer.Close
		    Session.pointeurAllProjetContainer = Nil
		    Session.pointeurFormulaireContainerControl  = Nil
		  End If
		  
		  Dim hauteurPanneauProjetSideBar As Integer = Me.Height
		  Dim largeurPanneauProjetSideBar As Integer = Me.Width
		  
		  Select Case panneauAafficher
		    
		  Case "General" // Panneau général
		    // Autoriser la mise à jour
		    IVModProjet.Visible = True
		    IVEnrProjet.Visible = False
		    // Afficher le panneau général
		    Dim projetContainer As ProjetContainer = New ProjetContainer
		    Session.pointeurAllProjetContainer = projetContainer
		    projetContainer.EmbedWithin(Session.pointeurProjetContainerControl.ProjetAllProjetContainer, 0, 0, 1000, hauteurPanneauProjetSideBar)
		    projetContainer.LockTop = True
		    projetContainer.LockBottom = True
		    projetContainer.LockLeft = True
		    projetContainer.LockRight = True
		    projetContainer.prefix = "projet"
		    projetContainer.tableName = "projet"
		    sProjet = Nil
		    sProjet = New ProjetClass()
		    projetContainer.listProjetDetail
		    
		  Case "Formulaire" // Panneau formulaire
		    // Empêcher la mise à jour
		    IVModProjet.Visible = False
		    // Afficher le panneau formulaire
		    Dim formulaireContainerControl As FormulaireContainerControl = New FormulaireContainerControl
		    Session.pointeurAllProjetContainer = formulaireContainerControl
		    Session.pointeurFormulaireContainerControl = formulaireContainerControl
		    formulaireContainerControl.EmbedWithin(Session.pointeurProjetContainerControl.ProjetAllProjetContainer, 0, 0, 1000, hauteurPanneauProjetSideBar)
		    formulaireContainerControl.LockTop = True
		    formulaireContainerControl.LockBottom = True
		    formulaireContainerControl.LockLeft = True
		    formulaireContainerControl.LockRight = True
		    Dim aaa As String = ProjetListBox.Cell(ProjetListBox.ListIndex, 0)
		    Session.pointeurFormulaireContainerControl.FormulaireSideBarDialog.projet_numero = ProjetListBox.Cell(ProjetListBox.ListIndex, 0)
		    formulaireContainerControl.FormulaireSideBarDialog.populateFormulaire
		  End Select 
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function genClauseWhere(listeSelection_param As Dictionary) As String
		  
		  Dim listeSelection As String = ""
		  Dim keys(), k, v as variant
		  
		  keys = listeSelection_param.keys()
		  for each k in keys
		    Select Case k
		      
		    Case "DateDebut"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and projet_date >= '" + v + "'  "
		      
		    Case "DateFin"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and projet_date <= '" + v + "'  "
		      
		    Case "Projet"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and projet_projet IN('" + v + "')  "
		      
		    Case "Revision"
		      v = listeSelection_param.value(k)
		      listeSelection = listeSelection + "and projet_revision IN(" + v + ")  "
		      
		    End Select
		    
		  Next
		  
		  Return listeSelection
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub implementLock()
		  Dim ptrProjetCont As ProjetContainer = ProjetContainer(Session.pointeurAllProjetContainer)
		  Dim message As String = ""
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim param_recherche As String = PROJETSCHEMA + "." + PROJETTABLENAME + "." + str(Self.sProjet.projet_id)
		  Message = Self.sProjet.check_If_Locked(Session.bdTechEol, param_recherche)
		  If  Message <> "Locked" And Message <> "Unlocked" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If Message = "Locked" Then
		    Session.pointeurProjetContainerControl.IVLockedProjet.Visible = True
		    Session.pointeurProjetContainerControl.TLLockedByProjet.Text = Self.sProjet.lockedBy
		    Session.pointeurProjetContainerControl.TLLockedByProjet.Visible = True
		    Exit Sub
		  End If
		  
		  //Déverrouiller l'enregistrement si on est actuellement en Verrouillage
		  If Self.sProjet.lockEditMode = True Then
		    message = Self.sProjet.unLockRow(Session.bdTechEol)
		    If  message <> "Succes" Then
		      afficherMessage(message)
		      Exit Sub
		    End If
		    IVModProjet.Picture = ModModal30x30
		    // Rendre disponible l'ajout et la suppression
		    //IVAddProjet.Visible = True
		    //IVAddProjet.Enabled = True
		    //IVDelProjet.Visible = True
		    //IVDelProjet.Enabled = True
		    // Libérer 
		    Session.pointeurProjetContainerControl.IVLockedProjet.Visible = False
		    Session.pointeurProjetContainerControl.TLLockedByProjet.Visible = False
		    IVEnrProjet.Visible = False
		    IVEnrProjet.Enabled = False
		    // Bloquer les zones du container
		    Self.sProjet.lockEditMode = False
		    ptrProjetCont.listProjetDetail
		    Exit Sub
		  End If
		  
		  //Activer le mode Verrouillage
		  Self.sProjet.lockEditMode = True
		  
		  //Creer les valeurs de verrouillage record ID
		  Self.sProjet.recordID = PROJETSCHEMA + "." + PROJETTABLENAME + "." + str(Self.sProjet.projet_id)
		  message = Self.sProjet.lockRow(Session.bdTechEol) 
		  If message <> "Succes" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  Session.pointeurProjetContainerControl.IVLockedProjet.Visible = False
		  Session.pointeurProjetContainerControl.TLLockedByProjet.Visible = False
		  IVEnrProjet.Enabled = Self.sProjet.recordAvailable
		  
		  //Activer les fonctions pertinentes
		  IVModProjet.Picture = ModModalOrange30x30
		  IVAddProjet.Visible = False
		  IVAddProjet.Enabled = False
		  IVDelProjet.Visible = False
		  IVDelProjet.Enabled = False
		  IVEnrProjet.Visible = True
		  IVEnrProjet.Enabled = True
		  
		  // Débloquer les zones du container
		  ptrProjetCont.listProjetDetail
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub modProjetDetail()
		  
		  Dim ptrProjetCont As ProjetContainer = ProjetContainer(Session.pointeurAllProjetContainer)
		  ptrProjetCont.traiteMAJ
		  // Forcer le réaffichage
		  changementProjet(ProjetListBox.ListIndex)
		  
		  Dim ML6MajModalBox As New MajModal
		  ML6MajModalBox.Show
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populateProjet(Optional projet_id_param as Integer)
		  
		  If Self.sProjet = Nil Then
		    Self.sProjet = new ProjetClass(Session.bdTechEol, Self.PROJETTABLENAME, Self.PROJETPREFIX)
		  End If
		  
		  Dim projetRS As RecordSet
		  projetRS = Self.sProjet.loadData(Session.bdTechEol, Self.PROJETTABLENAME, "projet_no")
		  
		  ProjetListBox.DeleteAllRows
		  
		  If projetRS = Nil Then Exit Sub
		  
		  Dim index As Integer = 0
		  For i As Integer = 1 To projetRS.RecordCount
		    If (Me.ProjetEnCoursCheckbox.Value = False  And Me.ProjetAPlanifierCheckbox.Value = False) OR _
		      (projetRS.Field("projet_statut").StringValue = "Travaux en cours" And Me.ProjetEnCoursCheckbox.Value = True)  OR _
		      (projetRS.Field("projet_statut").StringValue = "Travaux à planifier" And Me.ProjetAPlanifierCheckbox.Value = True) Then
		      
		      ProjetListBox.AddRow(projetRS.Field("projet_no").StringValue, projetRS.Field("projet_titre").StringValue)
		      
		      //If projetRS.Field("projet_statut").StringValue <> "A" Then
		      //For j As Integer = 0 To 3
		      //ProjetListBox.CellStyle(i-1, j) = BGGray200
		      //Next
		      //End If
		      
		      // Garder le numéro de ligne
		      ProjetListBox.RowTag(ProjetListBox.LastIndex) = index
		      // Garder l'id numéro du projet
		      ProjetListBox.CellTag(ProjetListBox.LastIndex,1) = projetRS.Field("projet_id").IntegerValue
		      index = index + 1
		    End If
		    projetRS.MoveNext
		  Next
		  
		  projetRS.Close
		  projetRS = Nil
		  
		  // Si la liste est vide, sortir
		  If ProjetListBox.RowCount <= 0 Then Exit Sub
		  
		  
		  // Positionnement par rapport à un numéro en particulier
		  If projet_id_param <> 0 Then
		    For indexListBox As Integer = 0 To ProjetListBox.RowCount - 1
		      If ProjetListBox.CellTag(indexListBox,1) = projet_id_param Then
		        Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.no_ligne = indexListBox
		        ProjetListBox.ListIndex  = indexListBox
		        Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.id= ProjetListBox.CellTag(Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.no_ligne,1)
		        Exit Sub
		      End If
		    Next
		    Exit Sub
		  End If
		  
		  // Positionnement à la ligne gardée en mémoire
		  If ProjetListBox.ListIndex  <> Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.no_ligne And ProjetListBox.RowCount > 0 Then
		    Dim v As Variant = ProjetListBox.LastIndex
		    v = Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.no_ligne
		    If Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.no_ligne > ProjetListBox.LastIndex  Then 
		      Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.no_ligne = ProjetListBox.LastIndex
		    End If
		    ProjetListBox.ListIndex  = Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.no_ligne
		    v = Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.no_ligne
		    v = ProjetListBox.CellTag(Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.no_ligne,1)
		    Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.id = ProjetListBox.CellTag(Session.pointeurProjetContainerControl.ProjetSideBarDialog.projetStruct.no_ligne,1)
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub supProjetDetail()
		  Dim messageErreur As String = "Succes"
		  
		  //Déterminer sur quel projet on est
		  Dim projetActuel As String = ProjetListBox.CellTag(ProjetListBox.ListIndex,2)
		  Dim dBSchemaName As String = "form"
		  Dim dBTableName As String = ""
		  Dim dBPrefix As String = ""
		  
		  Select Case projetActuel
		  Case "9FA6D6AE78" // Projet Meeting SST Journalier
		    dBTableName = "fss0023_meeting_sst_journ"
		    dBPrefix = "fss0023"
		  Case "46B73282F9" // Projet Vérification hebdomadaire chef de chantier
		    dBTableName = "fss0006_verif_hebdo_chef_chantier"
		    dBPrefix = "fss0006"
		  Case "C56E5F0670" // Projet Rapport visite de chantier
		    dBTableName = "rop0008_rapport_visite_chantier"
		    dBPrefix = "rop0009"
		  Case "9004B3B2CC" // Projet Rapport hebdomadaire
		    dBTableName = "rop0009_rapport_hebdomadaire"
		    dBPrefix = "rop0009"
		  End Select 
		  
		  // Vérifier si un enregistrement peut être supprimé
		  messageErreur = Self.sProjet.validerSuppression(Session.bdTechEol, dBTableName, str(Self.sProjet.projet_id))
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    //pointeurProbMsgModal.WLAlertLabel.Text = ProjetModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit Sub
		  End If
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim param_recherche As String =dBSchemaName + "." +dBTableName+ "." + str(ProjetListBox.CellTag(ProjetListBox.ListIndex,1))
		  messageErreur = Self.sProjet.check_If_Locked(Session.bdTechEol, param_recherche)
		  If  messageErreur <> "Locked" And messageErreur <> "Unlocked" Then
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If messageErreur = "Locked" Then
		    Session.pointeurProjetContainerControl.IVLockedProjet.Visible = True
		    Session.pointeurProjetContainerControl.TLLockedByProjet.Text = Self.sProjet.lockedBy
		    Session.pointeurProjetContainerControl.TLLockedByProjet.Visible = True
		    Exit Sub
		  End If
		  
		  // L'id  est contenu dans la variable ProjetListBox.CellTag(ProjetListBox.ListIndex,1))
		  messageErreur = Self.sProjet.supprimerDataByField(Session.bdTechEol, dBTableName, dBPrefix + "_id",str(ProjetListBox.CellTag(ProjetListBox.ListIndex,1)))
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    //pointeurProbMsgModal.WLAlertLabel.Text = ProjetModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		  Else
		    // Réafficher la liste des projets
		    Self.populateProjet
		  End If
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		panneauAafficher As String = "General"
	#tag EndProperty

	#tag Property, Flags = &h0
		projetStruct As ProjetStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		sProjet As ProjetClass
	#tag EndProperty


	#tag Constant, Name = PROJETPREFIX, Type = String, Dynamic = False, Default = \"projet", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PROJETSCHEMA, Type = String, Dynamic = False, Default = \"dbglobal", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PROJETTABLENAME, Type = String, Dynamic = False, Default = \"projet", Scope = Public
	#tag EndConstant


	#tag Structure, Name = ProjetStructure, Flags = &h0
		projet As String*12
		  date As String*100
		  projet_form_id As String*10
		  id As Integer
		  no_ligne As Integer
		  edit_mode As String*30
		  table_nom As String*30
		revision As String*1
	#tag EndStructure


#tag EndWindowCode

#tag Events ProjetListBox
	#tag Event
		Sub SelectionChanged()
		  If Me.ListIndex < 0 Then
		    // Rien n'a été sélectionné, réinitialiser les champs
		    //ClearProjetsFields
		    //EnableFields(False)
		    
		  Else
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    Session.pointeurProjetContainerControl.ProjetSideBarDialog.ProjetStruct.no_ligne = Me.RowTag(Me.ListIndex)
		    Session.pointeurProjetContainerControl.ProjetSideBarDialog.ProjetStruct.id = Me.CellTag(Me.ListIndex,1)
		    changementProjet(Me.ListIndex)
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVAddProjet
	#tag Event
		Sub MouseExit()
		  //Self.IVAddProjet.Picture = AjouterModal30x30
		  Self.IVDelProjet.Picture = DeleteModal30x30
		  Self.IVEnrProjet.Picture = Enregistrer30x30
		  Self.IVModProjet.Picture = ModModal30x30
		  Self.IVRafProjet.Picture = RefreshMenu30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  //Self.IVAddProjet.Picture = AjouterModalTE30x30
		  Self.IVDelProjet.Picture = DeleteModal30x30
		  Self.IVEnrProjet.Picture = Enregistrer30x30
		  Self.IVModProjet.Picture = ModModal30x30
		  Self.IVRafProjet.Picture = RefreshMenu30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If Me.Enabled Then
		    ajouterProjet
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVDelProjet
	#tag Event
		Sub MouseExit()
		  Self.IVAddProjet.Picture = AjouterModal30x30
		  Self.IVDelProjet.Picture = DeleteModal30x30
		  Self.IVEnrProjet.Picture = Enregistrer30x30
		  Self.IVModProjet.Picture = ModModal30x30
		  Self.IVRafProjet.Picture = RefreshMenu30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddProjet.Picture = AjouterModal30x30
		  Self.IVDelProjet.Picture = DeleteModalTE30x30
		  Self.IVEnrProjet.Picture = Enregistrer30x30
		  Self.IVModProjet.Picture = ModModal30x30
		  Self.IVRafProjet.Picture = RefreshMenu30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If Me.Enabled Then
		    supProjetDetail
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVEnrProjet
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  modProjetDetail
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Self.IVAddProjet.Picture = AjouterModal30x30
		  Self.IVDelProjet.Picture = DeleteModal30x30
		  Self.IVEnrProjet.Picture = Enregistrer30x30
		  Self.IVModProjet.Picture = ModModal30x30
		  Self.IVRafProjet.Picture = RefreshMenu30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddProjet.Picture = AjouterModal30x30
		  Self.IVDelProjet.Picture = DeleteModal30x30
		  Self.IVEnrProjet.Picture = EnregistrerTE30x30
		  Self.IVModProjet.Picture = ModModal30x30
		  Self.IVRafProjet.Picture = RefreshMenu30x30
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVModProjet
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		  //Déclenchement du mode édition et déverrouillage ou verrouillage
		  If ProjetListBox.ListIndex <> -1 And ProjetListBox.RowCount > 0 Then
		    implementLock
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Self.IVAddProjet.Picture = AjouterModal30x30
		  Self.IVDelProjet.Picture = DeleteModal30x30
		  Self.IVEnrProjet.Picture = Enregistrer30x30
		  Self.IVRafProjet.Picture = RefreshMenu30x30
		  //If Self.sProjet.lockEditMode = False Then
		  //Self.IVModProjet.Picture = ModModal30x30
		  //Else
		  //Self.IVModProjet.Picture = ModModalOrange30x30
		  //End If
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddProjet.Picture = AjouterModal30x30
		  Self.IVDelProjet.Picture = DeleteModal30x30
		  Self.IVEnrProjet.Picture = Enregistrer30x30
		  Self.IVModProjet.Picture = ModModalTE30x30
		  Self.IVRafProjet.Picture = RefreshMenu30x30
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVRafProjet
	#tag Event
		Sub MouseExit()
		  Me.Picture = RefreshMenu30x30
		  IVAddProjet.Picture = AjouterModal30x30
		  IVModProjet.Picture = ModModal30x30
		  IVDelProjet.Picture = DeleteModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Me.Picture = RefreshMenuOra30x30
		  IVAddProjet.Picture = AjouterModal30x30
		  IVModProjet.Picture = ModModal30x30
		  IVDelProjet.Picture = DeleteModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  populateProjet
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ProjetEnCoursCheckbox
	#tag Event
		Sub ValueChanged()
		  // Générer la liste des employés
		  populateProjet
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ProjetAPlanifierCheckbox
	#tag Event
		Sub ValueChanged()
		  // Générer la liste des employés
		  populateProjet
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="panneauAafficher"
		Group="Behavior"
		InitialValue="General"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
